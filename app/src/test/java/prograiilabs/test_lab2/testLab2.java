package prograiilabs.test_lab2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import prograiilabs.Lab_1.Actividad_1.Activity_1;
import prograiilabs.Lab_1.Actividad_2.*;
import prograiilabs.Lab_2.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class testLab2 {

    @Test
    public void testCrearEvento() {
        // Arrange
        Organizador organizador = new Organizador("12345", "Nombre Organizador", "correo@organizador.com",
                "Dirección Organizador", "123456789", "usuarioOrganizador", "password", "Organizador", "Razón Social",
                "Descripción");

        // Act
        Evento eventoCreado = organizador.crearEvento("EV001", "Nombre Evento", new Ubicacion("Lugar del evento","colombia","cesar","aguachica"),
                "2023-07-25", new Categoria(), "Cartel de artistas", "Programa del evento", new ArrayList<>());

        // Assert
        assertNotNull(eventoCreado);
        assertEquals("EV001", eventoCreado.getCodigo());
        assertEquals("Nombre Evento", eventoCreado.getNombre());
    }

    @Test
    public void testConstructorArtista() {
        // Arrange
        Persona artista = new Artista("987654321", "Nombre Artista", "artista@correo.com", "Dirección Artista", "123456789","persona", "Descripción del artista");

        // Assert
        assertNotNull(artista);
        assertEquals("987654321", artista.getDocumento());
        assertEquals("Nombre Artista", artista.getNombre());
        assertEquals("artista@correo.com", artista.getCorreoElectronico());
        assertEquals("Dirección Artista", artista.getDireccion());
        assertEquals("123456789", artista.getTelefono());
    }
}
