package prograiilabs;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import prograiilabs.Lab_1.Actividad_3.Students;
import prograiilabs.Lab_1.Actividad_3.University;

import java.util.ArrayList;
import java.util.List;

public class UniversityTest {
    private University university;

    @BeforeEach
    public void setup() {
        university = new University();
        // Agregar estudiantes de prueba
        List<String> courses1 = new ArrayList<>();
        courses1.add("maths");
        courses1.add("science");
        Students student1 = new Students("Raul", 2, courses1);
        university.AddStudents(student1);

        List<String> courses2 = new ArrayList<>();
        courses2.add("physics");
        courses2.add("chemistry");
        Students student2 = new Students("Luis", 1, courses2);
        university.AddStudents(student2);

        List<String> courses3 = new ArrayList<>();
        courses3.add("history");
        courses3.add("literature");
        courses3.add("geography");
        Students student3 = new Students("Liz", 3, courses3);
        university.AddStudents(student3);
    }

    @Test
    public void testPromover() {
        String name = "Raul";
        int newGrade = 3;
        university.promover(name, newGrade);

        Assertions.assertEquals(newGrade, university.students.get(0).getGrade());
    }

    @Test
    public void testPromoverEstudianteNoEncontrado() {
        String name = "Juan";
        int newGrade = 3;
        university.promover(name, newGrade);

        Assertions.assertNotEquals(newGrade, university.students.get(0).getGrade());
    }

    @Test
    public void testAddCourse() {
        String name = "Luis";
        String newCourse = "biology";
        university.addCourse(name, newCourse);

        Assertions.assertTrue(university.students.get(1).getCourses().contains(newCourse));
    }

    @Test
    public void testAddCourseEstudianteNoEncontrado() {
        String name = "Maria";
        String newCourse = "biology";
        university.addCourse(name, newCourse);

        Assertions.assertFalse(university.students.get(0).getCourses().contains(newCourse));
    }

    @Test
    public void testFilterCourse() {
        String course = "literature";
        List<Students> filteredStudents = new ArrayList<>();
        filteredStudents.add(university.students.get(2));

        Assertions.assertEquals(filteredStudents, university.filterCourse(course));
    }
    @Test
    public void testFilterCourseNoStudentsFound() {
        String course = "biologia";
        List<Students> filteredStudents = new ArrayList<>();

        Assertions.assertEquals(filteredStudents, university.filterCourse(course));
    }

    @Test
    public void testListStudents() {
        String expectedOutput = "Name: Raul, Grade: 2, courses: maths, science\n" +
                "Name: Luis, Grade: 1, courses: physics, chemistry\n" +
                "Name: Liz, Grade: 3, courses: history, literature, geography\n";

        Assertions.assertEquals(expectedOutput, university.listStudents());
    }
}
