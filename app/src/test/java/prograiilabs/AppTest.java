package prograiilabs;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import prograiilabs.Lab_1.Actividad_1.Activity_1;
import prograiilabs.Lab_1.Actividad_2.*;

import java.util.ArrayList;

/**
 * This is the AppTest class.
 */
class AppTest {
  @Test
  void appHasGreeting() {
    App classUnderTest = new App();
    assertNotNull(classUnderTest.getGreeting(), "app should have a greeting");
  }

  //test del cuadrante
  @Test
  public void testDeterminarCuadrante() {

    // Caso de prueba 1
    int cuadrante1 = Activity_1.determinarCuadrante(-12, 5);
    Assertions.assertEquals(1, cuadrante1);

    // Caso de prueba 2
    int cuadrante2 = Activity_1.determinarCuadrante(12, 5);
    Assertions.assertEquals(2, cuadrante2);

    // Caso de prueba 3
    int cuadrante3 = Activity_1.determinarCuadrante(-12, -5);
    Assertions.assertEquals(3, cuadrante3);

    // Caso de prueba 4
    int cuadrante4 = Activity_1.determinarCuadrante(12, -5);
    Assertions.assertEquals(4, cuadrante4);
  }

  //test del factorial
  @Test
  public void testCalcular() {

    // Caso de prueba 1
    int[] input1 = {1,2,3};
    int[] output1 = {1,2,6};
    Assertions.assertArrayEquals(output1, Activity_1.factorial(input1));

    // Caso de prueba 2
    int[] input2 = {5, 2};
    int[] output2 = {0, 2};
    Assertions.assertArrayEquals(output2, Activity_1.factorial(input2));
  }

  //test del circulo
  @Test
  public void testCirculo() {
    // Crear un objeto Cuadrado
    Circulo circle = new Circulo(5);

    // Verificar el área y el perímetro del circulo
    Assertions.assertEquals(31.42, circle.calcularPerimetro());
    Assertions.assertEquals(78.54, circle.calcularArea());
  }

  //test del cuadrado
  @Test
  public void testCuadrado() {
    // Crear un objeto Cuadrado
    Cuadrado cuadrado = new Cuadrado(2);

    // Verificar el área y el perímetro del cuadrado
    Assertions.assertEquals(4.0, cuadrado.calcularArea());
    Assertions.assertEquals(8.0, cuadrado.calcularPerimetro());
  }

  //test rectangulo
  @Test
  public void testRectangulo() {
    // Crear un objeto Rectangulo
    Rectangulo rectangulo = new Rectangulo(15, 7);

    // Verificar el área y el perímetro del rectángulo
    Assertions.assertEquals(105.0, rectangulo.calcularArea());
    Assertions.assertEquals(44.0, rectangulo.calcularPerimetro());
  }

  //test triangulos

  //test triangulo equilatero
  @Test
  public void testTrianguloEquilatero() {
    // Crear un objeto TrianguloEquilatero
    TrianguloEquilatero triangulo = new TrianguloEquilatero(5.0);

    // Verificar el área y el perímetro del triángulo equilátero
    Assertions.assertEquals(10.83, triangulo.calcularArea(), 0.01);
    Assertions.assertEquals(15.0, triangulo.calcularPerimetro());
  }

  //test triangulo escaleno
  @Test
  public void testTrianguloEscaleno() {
    // Crear un objeto TrianguloEscaleno
    TrianguloEscaleno triangulo = new TrianguloEscaleno(3.0, 4.0, 5.0);

    // Verificar el área y el perímetro del triángulo escaleno
    Assertions.assertEquals(6.0, triangulo.calcularArea());
    Assertions.assertEquals(12.0, triangulo.calcularPerimetro());
  }

  //test triangulo isoscelos
  @Test
  public void testTrianguloIsosceles() {
    // Crear un objeto TrianguloIsosceles
    TrianguloIsosceles triangulo = new TrianguloIsosceles(5.0, 7.0);

    // Verificar el área y el perímetro del triángulo isósceles
    Assertions.assertEquals(16.35, triangulo.calcularArea());
    Assertions.assertEquals(19.0, triangulo.calcularPerimetro());
  }
}
