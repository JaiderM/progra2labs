package prograiilabs.Lab_1.Actividad_2;

public class TrianguloEquilatero extends Triangulo {
    public TrianguloEquilatero(double lado) {
        super(lado, lado, lado);
    }

    public double calcularArea() {
        double s = (lado1 + lado2 + lado3) / 2;
        return (double) Math.round((Math.sqrt(3) / 4 * lado1 * lado1) * 100) /100;
    }

    public double calcularPerimetro() {
        return (double) Math.round((lado1 + lado2 + lado3) * 100) /100;
    }

    @Override
    public String toString(){
        return "TrianguloEquilatero \nPerimetro = "+ calcularPerimetro() +"\n"+ "Area = "+ calcularArea();
    }
}
