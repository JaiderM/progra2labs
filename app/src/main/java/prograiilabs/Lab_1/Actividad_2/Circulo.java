package prograiilabs.Lab_1.Actividad_2;

public class Circulo implements Figura {
    private double radio;

    public Circulo(double radio) {
        this.radio = radio;
    }

    public double calcularArea() {
        return (double) Math.round((Math.PI * radio * radio) * 100) /100;
    }

    public double calcularPerimetro() {
        return (double) Math.round(( 2 * Math.PI * radio) * 100) /100;
    }

    @Override
    public String toString(){
        return "Circulo \nPerimetro = "+ calcularPerimetro() +"\n"+ "Area = "+ calcularArea();
    }
}
