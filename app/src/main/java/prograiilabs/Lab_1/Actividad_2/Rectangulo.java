package prograiilabs.Lab_1.Actividad_2;

public class Rectangulo implements Figura {
    private double base;
    private double altura;

    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public double calcularArea() {
        return (double) Math.round((base * altura) * 100) /100;
    }

    public double calcularPerimetro() {
        return (double) Math.round((2 * (base + altura)) * 100) /100;
    }

    @Override
    public String toString(){
        return "Rectangulo \nPerimetro = "+ calcularPerimetro() +"\n"+ "Area = "+ calcularArea();
    }
}
