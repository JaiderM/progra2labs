package prograiilabs.Lab_1.Actividad_2;

public class TrianguloIsosceles extends Triangulo {
    public TrianguloIsosceles(double lado1, double lado2) {
        super(lado1, lado2, lado2);
    }

    public double calcularArea() {
        return (double) Math.round(((lado1 * Math.sqrt(lado2 * lado2 - (lado1 * lado1 / 4))) / 2) * 100) /100;
    }

    public double calcularPerimetro() {
        return (double) Math.round((lado1 + lado2 + lado3) * 100) /100;
    }

    @Override
    public String toString(){
        return "TrianguloIsosceles \nPerimetro = "+ calcularPerimetro() +"\n"+ "Area = "+ calcularArea();
    }
}
