package prograiilabs.Lab_1.Actividad_2;

// Superclase o interfaz para calcular el área y perímetro de una figura
interface Figura {
    double calcularArea();

    double calcularPerimetro();
}
