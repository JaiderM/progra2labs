package prograiilabs.Lab_1.Actividad_2;

public class TrianguloEscaleno extends Triangulo {
    public TrianguloEscaleno(double lado1, double lado2, double lado3) {
        super(lado1, lado2, lado3);
    }

    public double calcularArea() {
        double s = (lado1 + lado2 + lado3) / 2;
        return (double) Math.round((Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3))) * 100) /100;
    }

    public double calcularPerimetro() {
        return (double) Math.round((lado1 + lado2 + lado3) * 100) /100;
    }

    @Override
    public String toString(){
        return "TrianguloEscaleno \nPerimetro = "+ calcularPerimetro() +"\n"+ "Area = "+ calcularArea();
    }
}
