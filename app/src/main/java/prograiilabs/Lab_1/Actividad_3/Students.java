package prograiilabs.Lab_1.Actividad_3;

import java.util.List;

public class Students {
    private String name;
    private int grade;
    private List<String> courses;

    public Students(String name, int grade, List<String> courses) {
        this.name = name;
        this.grade = grade;
        this.courses = courses;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public void addCourse(String course) {
        courses.add(course);
    }
}
