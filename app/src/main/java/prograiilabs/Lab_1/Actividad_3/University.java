package prograiilabs.Lab_1.Actividad_3;

import java.util.ArrayList;
import java.util.List;

public class University {
    public List<Students> students;

    public University() {
        students = new ArrayList<>();
    }

    public void AddStudents(Students student){
        students.add(student);
    }

    public void promover(String name, int newGrade){
        for (Students student : students) {
            if (student.getName().equals(name)) {
                student.setGrade(newGrade);
                System.out.println("Promoted student " + name + " to grade " + newGrade);
                return;
            }
        }
        System.out.println("Student " + name + " not found");
    }

    public void addCourse(String name, String newCourse){
        for (Students student : students) {
            if (student.getName().equals(name)) {
                student.addCourse(newCourse);
                System.out.println("Assigned new course " + newCourse + " to student " + name);
                return;
            }
        }
        System.out.println("Student " + name + " not found");
    }

    public List<Students> filterCourse(String Course){
        List<Students> studentsWithCourse = new ArrayList<>();

        for (Students student : students) {
            if (student.getCourses().contains(Course))
                studentsWithCourse.add(student);
        }

        if (studentsWithCourse.isEmpty()) {
            System.out.println("No students found with the course: " + Course);
        } else {
            System.out.println("Students with the course " + Course + ":");
            for (Students student : studentsWithCourse) {
                System.out.println("Name: " + student.getName() + ", Grade: " + student.getGrade());
            }
        }
        return studentsWithCourse;
    }

    public String listStudents() {
        StringBuilder sb = new StringBuilder();
        for (Students student : students) {
            String coursesFormatted = String.join(", ", student.getCourses());
            sb.append("Name: ").append(student.getName())
                    .append(", Grade: ").append(student.getGrade())
                    .append(", courses: ").append(coursesFormatted)
                    .append("\n");
        }
        return sb.toString();
    }
}
