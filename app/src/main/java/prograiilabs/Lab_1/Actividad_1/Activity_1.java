package prograiilabs.Lab_1.Actividad_1;

public class Activity_1 {

    public static int determinarCuadrante(int x, int y) {

        if (x < 0 && y > 0)
            return 1;
        else if (x > 0 && y > 0)
            return 2;
        else if (x < 0 && y < 0)
            return 3;
        else
            return 4;
    }

    public static int [] factorial(int [] values){

        for (int i = 0; i < values.length; i++) {
            int result = 1;
            for (int j = 2; j <= values[i]; j++)
                result = (result * j) % 10;
            values[i] = result;
        }
        return values;
    }
}
