package prograiilabs.Lab_2;

import java.util.List;

public class Entrada {

    private Evento evento;
    private TipoEntrada tipo;
    private int cantidad;
    private List<String> asientosSeleccionados;
    private String informacionPago;
    private double precio;

    // Otras propiedades relevantes

    // Constructor, getters y setters


    public Entrada(Evento evento, TipoEntrada tipo, int cantidad, List<String> asientosSeleccionados, String informacionPago) {
        this.evento = evento;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.asientosSeleccionados = asientosSeleccionados;
        this.informacionPago = informacionPago;
    }

    public String entregarEntrada() {
        return "¡Gracias por su compra! Su boleto para el evento en " + evento.getUbicacion() + " ha sido entregado.";
    }
}
