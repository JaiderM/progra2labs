package prograiilabs.Lab_2;

public interface Crud_User {

    void CreateUsuario();
    void ReadUsuario();
    void UpdateUsuario();
    void DeleteUsuario();

}
