package prograiilabs.Lab_2;

import java.util.List;

public class Cartelera {

    private Ubicacion informacionLugar;
    private List<Artista> artistas;
    private ProgramaEvento programaEvento;
    // Otros atributos relacionados con la cartelera

    public Cartelera(Ubicacion informacionLugar, List<Artista> artistas, ProgramaEvento programaEvento) {
        this.informacionLugar = informacionLugar;
        this.artistas = artistas;
        this.programaEvento = programaEvento;
    }

    public Ubicacion getInformacionLugar() {
        return informacionLugar;
    }

    public List<Artista> getArtistas() {
        return artistas;
    }

    public ProgramaEvento getProgramaEvento() {
        return programaEvento;
    }

    // Otros métodos y atributos relevantes
}
