package prograiilabs.Lab_2;

public class ProgramaEvento {

    private String descripcion;
    // Otros atributos relacionados con el programa del evento

    public ProgramaEvento(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    // Otros métodos y atributos relevantes
}
