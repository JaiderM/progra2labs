package prograiilabs.Lab_2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Evento implements BuscadorEventos{

    private String codigo;
    private String nombre;
    private Ubicacion ubicacion;
    private Date fecha;
    private Categoria categoria;
    private String cartelArtistas;
    private String programa;
    private List<TipoEntrada> TiposEntradas;

    public Evento(String codigo, String nombre, Ubicacion ubicacion, Date fecha, Categoria categoria,
                  String cartelArtistas, String programa, List<TipoEntrada> tiposEntradas) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.fecha = fecha;
        this.categoria = categoria;
        this.cartelArtistas = cartelArtistas;
        this.programa = programa;
        this.TiposEntradas = new ArrayList<>();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getCartelArtistas() {
        return cartelArtistas;
    }

    public void setCartelArtistas(String cartelArtistas) {
        this.cartelArtistas = cartelArtistas;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public List<TipoEntrada> getTiposEntradas() {
        return TiposEntradas;
    }

    public void setTiposEntradas(List<TipoEntrada> tiposEntradas) {
        TiposEntradas = tiposEntradas;
    }

    @Override
    public List<Evento> buscarPorUbicacion(String ubicacion) {
        return null;
    }

    @Override
    public List<Evento> buscarPorCategoria(String categoria) {
        return null;
    }

    @Override
    public List<Evento> buscarPorPalabrasClave(List<String> palabrasClave) {
        return null;
    }

    @Override
    public List<Evento> buscarPorFecha(Date Fecha) {
        return null;
    }
}
