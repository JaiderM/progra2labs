package prograiilabs.Lab_2;

import java.util.ArrayList;
import java.util.List;

public class User_Public extends Usuario implements Crud_User{

    private List<Evento> eventosFavoritos;
    private List<Entrada> historialCompras;

    public User_Public(String documento, String nombre, String correoElectronico, String direccion, String telefono, String user, String contrasena, String rol, List<Evento> eventosFavoritos, List<Entrada> historialCompras) {
        super(documento, nombre, correoElectronico, direccion, telefono, user, contrasena, rol);
        this.eventosFavoritos = new ArrayList<>();
        this.historialCompras = new ArrayList<>();
    }

    public void guardarEventoFavorito(Evento evento) {
        eventosFavoritos.add(evento);
    }

    public String recibirRecordatorios() {
        return "Se ha enviado un recordatorio al usuario " + this.getNombre() + " con el email " + getCorreoElectronico() + ".";
    }

    public Entrada comprarBoleto(Evento evento, TipoEntrada tipoEntrada, List<String> asientosSeleccionados, String informacionPago) {
        Entrada boleto = new Entrada(evento, tipoEntrada, 10 ,asientosSeleccionados, informacionPago);
        historialCompras.add(boleto);
        return boleto;
    }

    //Funcionalidades CRUD
    @Override
    public void CreateUsuario() {

    }
    @Override
    public void ReadUsuario() {

    }
    @Override
    public void UpdateUsuario() {

    }
    @Override
    public void DeleteUsuario() {

    }


}
