package prograiilabs.Lab_2;

public abstract class Persona {
    private String documento;
    private String nombre;
    private String correoElectronico;
    private String Direccion;
    private String Telefono;

    //constructor
    public Persona(String documento, String nombre, String correoElectronico, String direccion, String telefono) {
        this.documento = documento;
        this.nombre = nombre;
        this.correoElectronico = correoElectronico;
        this.Direccion = direccion;
        this.Telefono = telefono;
    }

    // métodos
    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }
}
