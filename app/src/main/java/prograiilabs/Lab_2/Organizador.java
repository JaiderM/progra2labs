package prograiilabs.Lab_2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Organizador extends Usuario implements Crud_User{
    private String razon_social;
    private String Descripcion;
    private List<Evento> eventosGestionados;

    public Organizador(String documento, String nombre, String correoElectronico, String direccion,
                       String telefono, String user, String contrasena, String rol, String razon_social,
                       String descripcion) {

        super(documento, nombre, correoElectronico, direccion, telefono, user, contrasena, rol);
        this.razon_social = razon_social;
        this.Descripcion = descripcion;
        this.eventosGestionados = new ArrayList<>();
    }

    public Evento crearEvento(String codigo,String nombre,Ubicacion ubicacion, String fecha, Categoria categoria,
                              String cartelArtistas, String programa,List <TipoEntrada> tipoEntradas) {
        Evento evento = new Evento(codigo,nombre,ubicacion,new Date(),categoria,cartelArtistas,programa,tipoEntradas);
        eventosGestionados.add(evento);
        return evento;
    }



    //Funcionalidades CRUD
    @Override
    public void CreateUsuario() {

    }
    @Override
    public void ReadUsuario() {

    }
    @Override
    public void UpdateUsuario() {

    }
    @Override
    public void DeleteUsuario() {

    }
}
