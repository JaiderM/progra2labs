package prograiilabs.Lab_2;

import java.util.Date;
import java.util.List;

public interface BuscadorEventos {
    List<Evento> buscarPorUbicacion(String ubicacion);
    List<Evento> buscarPorCategoria(String categoria);
    List<Evento> buscarPorPalabrasClave(List<String> palabrasClave);
    List<Evento> buscarPorFecha(Date Fecha);
}
