package prograiilabs.Lab_2;

public class Usuario extends Persona implements EnviarCorreo{

    private String user;
    private String contrasena;
    private String rol;

    public Usuario(String documento, String nombre, String correoElectronico, String direccion, String telefono, String user, String contrasena, String rol) {
        super(documento, nombre, correoElectronico, direccion, telefono);
        this.user = user;
        this.contrasena = contrasena;
        this.rol = rol;
    }

    @Override
    public void enviarCorreo(String destinatario, String asunto, String contenido) {
        // Implementar lógica para enviar un correo electrónico al destinatario
        System.out.println("Enviando correo a: " + destinatario + "\nAsunto: " + asunto + "\nContenido: " + contenido);

    }
}
