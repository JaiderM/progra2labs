package prograiilabs.Lab_2;

public class Artista extends Persona {

    private String Manager;
    private String Descripcion;

    public Artista(String documento, String nombre, String correoElectronico, String direccion, String telefono, String manager, String descripcion) {
        super(documento, nombre, correoElectronico, direccion, telefono);
        this.Manager = manager;
        this.Descripcion = descripcion;
    }
}
